import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../Screen/Home';
import Home1 from '../Screen/Home1';

const Stack = createNativeStackNavigator()

function NavigationApp() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Home1" component={Home1} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default NavigationApp;