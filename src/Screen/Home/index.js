import React, { Component } from 'react';
import { View, Text, Pressable } from 'react-native';
import styles from './styles';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  OnPress = (value) => {
    this.props.ChangeValue(value)
  }

  OnPressDelay = (value) => {
    setTimeout(() => {
      this.props.ChangeValue(value)
    }, 1000);
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
        <Text> App </Text>
        <View style={{ flexDirection: 'row' }}>
          <Pressable
            onPress={() => this.OnPress(-1)}
          >
            <View style={styles.viewBtn}><Text>-</Text></View>
          </Pressable>
          <View style={styles.viewValue}><Text>{this.props.dataValue}</Text></View>
          <Pressable
            onPress={() => this.OnPress(1)}
          >
            <View style={styles.viewBtn}><Text>+</Text></View>
          </Pressable>
        </View>
        <Pressable
          onPress={() => this.OnPressDelay(1)}
        >
          <View style={styles.viewBtnAnima}><Text>{"+<>"}</Text></View>
        </Pressable>

        <Pressable
          onPress={() => this.props.navigation.navigate("Home1")}
        >
          <View style={styles.viewBtnAnima}><Text>{"Go to Home1"}</Text></View>
        </Pressable>
      </View>
    );
  }
}

import { connect } from 'react-redux';
import { ChangeValue } from '../../redux/action';

const mapDispatchToProps = (dispatch) => {
  return {
    ChangeValue: (data) => {
      dispatch(ChangeValue(data))
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dataValue: state.RCount
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)