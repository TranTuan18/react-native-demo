import React, { Component } from 'react';
import { View, Text } from 'react-native';
import store from './src/redux/store';
import { Provider } from 'react-redux';

import Home from './src/Screen/Home';
import NavigationApp from './src/navigation';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Provider store={store}>
        <NavigationApp />
      </Provider>
    );
  }
}
export default App
